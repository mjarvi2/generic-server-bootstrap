#!/bin/bash
# Script to move file contents to the appropriate places for bootstrapping a server.

# Run this script as root
# Private git repo for the script that gets executed on every server
GIT_REPO="git@gitlab.com:mjarvi2/server-bootstrap-private.git"

# Set up runonce for root
echo '@reboot /usr/local/bin/runonce' > /var/spool/cron/crontabs/root
chown root:crontab /var/spool/cron/crontabs/root
chmod 600 /var/spool/cron/crontabs/root
cp -f runonce /usr/local/bin/runonce
chmod +x /usr/local/bin/runonce
mkdir -p /etc/local/runonce.d/ran

# Get the private git repo, which has 2 directories. scripts and files
# scripts gets placed in /usr/local/bin/runonce.d/ and files get placed in /etc/bootstrap/

git clone $GIT_REPO /etc/bootstrap/repo
rm -rf /etc/boostrap/repo/.git
mv /etc/bootstrap/repo/scripts/* /usr/local/bin/runonce.d/
mv /etc/bootstrap/repo/files/* /etc/bootstrap
rm -rf /etc/bootstrap/repo
